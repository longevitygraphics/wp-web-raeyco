<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

$product_data     = $product->get_data();
$attributes       = $product->get_attributes();
$attributes_keys  = array_keys( $attributes );
$product_image_id = $product->get_image_id();
$variant_table    = get_field( 'specifications' );

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'alignfull', $product ); ?>>

	<div class="product-wrapper">
		<div class="product__image" data-id=<?php echo esc_attr( $product_image_id ); ?>><?php woocommerce_show_product_images(); ?> </div>

		<div class="product__content">
			<div class="product__title"><h1><?php echo esc_html( $product->get_name() ); ?></h1></div>
			<div class="product__desc-short mb-2"><?php echo $product->get_short_description(); ?></div>
			<div class="product__desc-long"><?php echo esc_html( $product->get_description() ); ?></div>
			<div class="product__benefits mb-2">
				<h2>Benefits</h2>
				<?php if ( have_rows( 'benefits_list' ) ) : ?>
				<ul>
					<?php
					while ( have_rows( 'benefits_list' ) ) :
						the_row();
						?>
						<?php $benefit = get_sub_field( 'benefit' ); ?>
					<li><?php echo esc_html( $benefit ); ?></li>
				<?php endwhile; ?>
				</ul>
				<?php endif; ?>
					</div>
				<div class="product__add-to-cart">
					<?php woocommerce_template_single_add_to_cart(); ?>
				</div>
			</div>
		</div>
	
		<?php if ( have_rows( 'specifications' ) ) : ?>	
			<div class="variant-table">
			<h2>Which Model is Right for Me?</h2>
			<?php $index = 0; ?>
			<?php $count = count( $variant_table ); ?>
				<div class="variant-table__wrapper">
				<?php foreach ( $variant_table['header'] as $header ) : ?>
					<?php if ( $index === 1  ) : ?>
						<div class="swiper-container variant-table-swiper">
							<div class="swiper-wrapper">	
					<?php endif; ?>	
								<div class="variant-table__column <?php echo $index > 0 ? 'swiper-slide' : ''; ?>" data-index="<?php echo $index; ?>" >
									<div class="variant-table__header">	<?php echo $header['c']; ?>	</div>
										<?php foreach ( $variant_table['body'] as $body ) : ?>
											<div class="variant-table__row  ">
												<?php echo $body[ $index ]['c']; ?>
											</div>
										<?php endforeach; ?>

								</div>	
						<?php $index++; ?>
						<?php if ( $index === count( $variant_table['header'] )) : ?>
							</div>
	
							<div class="swiper-button-prev-item"></div>
							<div class="swiper-button-next-item"></div>

						</div>
					<?php endif; ?>	
				<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>




		<?php if ( have_rows( 'documents' ) ) : ?>
		<div class="product-files mt-4 mb-2">
			<h2>Documents</h2>
			<div class="files-wrapper">
			<?php

			while ( have_rows( 'documents' ) ) :
				the_row();
				?>
				<?php $file = get_sub_field( 'document' ); ?>
				<div class="file">
					<a href="<?php echo esc_url( $file['url'] ); ?>" target="_blank">
				
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/dist/images/pdf_icon.svg' ); ?>" width="100" height="100" alt="Download">
				<span><?php echo wp_kses_post( $file['title'] ); ?></span>`	
			</a>
				</div>
			<?php endwhile; ?>	
			</div>
		</div>
		<?php endif; ?>
		<div class="options-section">
			<?php
				$options_section_title = get_field( 'product_options_section_title', 'options' );
				$options_section_blurb = get_field( 'product_options_section_blurb', 'options' );
				$options_section_link  = get_field( 'product_options_section_link', 'options' )
			?>
			<h3 class="options-section__title"><?php echo esc_html( $options_section_title ); ?></h3>
			<div class="blurb-wrapper">
				<div class="options-section__blurb"><?php echo esc_html( $options_section_blurb ); ?></div>
				<div class="options-section__cta">
					<a class="btn btn-secondary" href="<?php echo esc_html( $options_section_link['url'] ); ?>" target="<?php echo esc_attr( $options_section_link['target'] ); ?>">
						<?php echo esc_html( $options_section_link['title'] ); ?>
					</a>
				</div>
			</div>
			
			<?php if ( have_rows( 'product_options_data', 'options' ) ) : ?>
				<div class="option-wrapper">
					<?php
					while ( have_rows( 'product_options_data', 'options' ) ) :
						the_row();
						?>
						<div class="option">
							<div class="option__title"><?php echo get_sub_field( 'title' ); ?></div>
							<div class="option__description"><?php echo get_sub_field( 'description' ); ?></div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>

		<div class="related-products py-4">

			<?php
			woocommerce_related_products(
				array(
					'posts_per_page' => -1,
					'columns'        => 4,
					'orderby'        => 'rand',
				)
			);
			?>


		</div>
	</div>


<?php do_action( 'woocommerce_after_single_product' ); ?>

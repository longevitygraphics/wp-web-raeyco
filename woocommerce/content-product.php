<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;


// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$product_data     = $product->get_data();
$attributes       = $product->get_attributes();
$attributes_keys  = array_keys( $attributes );
$product_image_id = $product->get_image_id();

?>
<li <?php wc_product_class( 'category-product-wrapper', $product ); ?>>
	<div class="lg-anim-reverse">
	<div class="product__image" data-id=<?php echo esc_attr( $product_image_id ); ?>><a href="<?php echo esc_url( $product->get_permalink() ); ?>"><?php echo wp_get_attachment_image( $product_image_id, 'lg_woocommerce_product_category' ); ?> </a></div>
	<div class="product__title"><h2><a href="<?php echo esc_url( $product->get_permalink() ); ?>"><?php echo esc_html( $product->get_name() ); ?></h2></a></div>
	<div class="product__desc-short mb-2"><?php echo esc_html( mb_strimwidth($product->get_short_description(), 0, 325, "..." ) ); ?></div>
	<div class="product__benefits mb-2">
		<h3>Benefits</h3>
		<?php if ( have_rows( 'benefits_list' ) ) : ?>
			<?php $i = 0; ?>
		<ul>
			<?php
			while ( have_rows( 'benefits_list' ) ) :
				the_row();
				?>
				<?php $i++; ?>
				<?php if ( $i > 3 ) : ?>
					<?php break; ?>
				<?php endif; ?>


				<?php $benefit = get_sub_field( 'benefit' ); ?>
			<li><?php echo esc_html( $benefit ); ?></li>
		<?php endwhile; ?>
		</ul>
		<?php endif; ?>
	</div>
	<div class="product__learn-more ">
		<a href="<?php echo esc_url( $product->get_permalink() ); ?>" class="btn btn-secondary btn-sm">Learn More</a>
	</div>
	<?php if ( !$product->is_type( 'simple' ) ) : ?>
	<hr class="hr-primary">
	<div class="product__variants">
		<h3>Models Available</h3>
		<?php echo $product->list_attributes(); ?>
	</div>
	<?php endif; ?>
	</div>
</li>

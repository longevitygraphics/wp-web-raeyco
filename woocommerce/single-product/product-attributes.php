<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-attributes.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

if ( ! $product_attributes ) {
	return;
}
?>
<div class="attributes-wrapper">
	<?php foreach ( $product_attributes as $product_attribute_key => $product_attribute ) : ?>
		<div class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo esc_attr( $product_attribute_key ); ?>">	
			<div class="woocommerce-product-attributes-item__value">
				<?php $attributes_string = $product_attribute['value']; ?>
				
				<?php $attributes = explode( ',', $attributes_string ); ?>
				<?php $attributes[0] = substr($attributes[0], 3) ?>
				<?php end($attributes);
					$key = key($attributes);
					reset($attributes);
					$attributes[$key] = trim($attributes[$key]);
					$attributes[$key] = substr($attributes[$key],0, -4);
					 
				?>
				<?php foreach ( $attributes as $attr ) : ?>
					<?php $attr = esc_html(trim($attr)) ?>
					<p><?php echo $attr; ?></p>

				<?php endforeach; ?>
			</div>
	</div>
	<?php endforeach; ?>
</div>

<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit941cf2ccd87281d1fd06ac03095ff704
{
    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInit941cf2ccd87281d1fd06ac03095ff704::$classMap;

        }, null, ClassLoader::class);
    }
}

<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

<?php
global $wp_query;
$current_page_title = ($wp_query and $wp_query->queried_object) ? $wp_query->queried_object->post_title : "";
$banner             = get_option( 'lg_option_blog_archive_banner_image' );
//$banner_height      = get_option( 'lg_option_blog_archive_banner_height' ) ? get_option( 'lg_option_blog_archive_banner_height' ) : '400px';
$blog_style         = get_option( 'lg_option_blog_style' ) ? get_option( 'lg_option_blog_style' ) : 'list';
?>

	<main class="blog">
		<?php if ( $banner ): ?>
			<div class="wp-block-cover alignfull page-header"
			     style="background-image:url(<?php echo $banner ?>)">
				<div class="wp-block-cover__inner-container">
					<h1 class="has-dark-color has-text-color"><?php echo $current_page_title ?></h1>
				</div>
			</div>
		<?php endif; ?>

		<div class="container">
			<?php
				switch ($blog_style) {
				    case "list":
				        get_template_part( 'templates/template-parts/blog/list');
				        break;
				    case "grid":
				        get_template_part( 'templates/template-parts/blog/grid');
				        break;
				    case "masonary":
				        get_template_part( 'templates/template-parts/blog/masonary');
				        break;
				}
			?>
		</div>
	</main>

<?php get_footer(); ?>

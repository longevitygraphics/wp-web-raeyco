<?php
/* Include theme functions */
$function_dir_path  = get_stylesheet_directory();
$function_file_path = array(
	'/libs/enqueue_styles_scripts.php',
	'/libs/theme_supports.php',
	//'/lg-woocommerce/lg-woocommerce.php',
	'/libs/functions.php',
	'/libs/pipette-calibration-request-form.php',
	'/libs/equipment-move-form.php',
	'/libs/equipment-service-request-form.php',
	'/libs/gravity-require-list-columns.php',
	'/libs/actions-hooks.php',

);

if ( $function_file_path && is_array( $function_file_path ) ) {
	foreach ( $function_file_path as $key => $value ) {
		if ( file_exists( $function_dir_path . $value ) ) {
			require_once $function_dir_path . $value;
		}
	}
}

add_filter(
	'doing_it_wrong_trigger_error',
	function () {
		return false;
	},
	10,
	0
);

/* end */

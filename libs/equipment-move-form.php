<?php
//pr(get_all_form_fields(6));
/*
 * Equipment Move Form
 * Manufacturer
 */
add_filter( 'gform_column_input_content_6_5_1', 'set_column_manufacturer', 10, 6 );
function set_column_manufacturer( $input, $input_info, $field, $text, $value, $form_id ) {
	//echo "<pre>";
	//pr( $field );
	//pr( $text );
	//die;
	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;

}

/*
 * Equipment Move Form
 * Model
 */
add_filter( 'gform_column_input_content_6_5_2', 'set_column_model', 10, 6 );
function set_column_model( $input, $input_info, $field, $text, $value, $form_id ) {
	//echo "<pre>";
	//pr( $field );
	//pr( $text );
	//die;
	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;

}

/*
 * Equipment Move Form
 * Dimensions: W-L-H (in)
 */
add_filter( 'gform_column_input_content_6_5_3', 'set_column_dimensions', 10, 6 );
function set_column_dimensions( $input, $input_info, $field, $text, $value, $form_id ) {
	//echo "<pre>";
	//pr( $field );
	//pr( $text );
	//die;
	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;

}

/*
 * Equipment Move Form
 * Weight (lbs)
 */
add_filter( 'gform_column_input_content_6_5_4', 'set_column_weight', 10, 6 );
function set_column_weight( $input, $input_info, $field, $text, $value, $form_id ) {
	//echo "<pre>";
	//pr( $field );
	//pr( $text );
	//die;
	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;

}

/*
 * Equipment Move Form
 * On Skid?
 */
add_filter( 'gform_column_input_6_5_5', 'set_column_on_skid', 10, 5 );
function set_column_on_skid( $input_info, $field, $column, $value, $form_id ) {
	$choices = array(
		'On Skid?',
		'Yes',
		'No',
	);
	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value )
	);

}

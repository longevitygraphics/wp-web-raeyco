<?php

function add_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Delta', '_s' ),
			'id'            => 'footer-delta',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

add_action( 'widgets_init', 'add_widgets_init' );


function remove_product_editor() {
	remove_post_type_support( 'product', 'editor' );
}

add_action( 'init', 'remove_product_editor' );


function bootstrap_classes_tinymce( $settings ) {
	$styles = array(
		array(
			'title' => 'None',
			'value' => ''
		),
		array(
			'title' => 'Table',
			'value' => 'table',
		),
		array(
			'title' => 'Striped',
			'value' => 'table table-striped ',
		),
		array(
			'title' => 'Bordered',
			'value' => 'table table-bordered',
		),
	);

	$settings['table_class_list'] = json_encode( $styles );

	return $settings;
}

add_filter( 'tiny_mce_before_init', 'bootstrap_classes_tinymce' );

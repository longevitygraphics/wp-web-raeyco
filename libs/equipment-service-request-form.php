<?php
/*
 * Equipment Service Request Form
 * I don't have a RAM#
 */
add_filter( 'gform_column_input_8_23_1', 'set_column_ram_dropdown', 10, 5 );
function set_column_ram_dropdown( $input_info, $field, $column, $value, $form_id ) {
	$choices = array(
		array( 'text' => 'I don\'t have a RAM# (Raeyco Asset Management No.)', 'value' => 'no' ),
		array( 'text' => 'I have a RAM# (Raeyco Asset Management No.)', 'value' => 'yes' ),
	);

	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value, false)
	);
}

/*
 * Equipment Service Request Form
 * RAM#
 */
add_filter( 'gform_column_input_content_8_23_2', 'set_column_ram', 10, 6 );
function set_column_ram( $input, $input_info, $field, $text, $value, $form_id ) {
	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;
}


/*
 * Equipment Service Request Form
 * Manufacturer
 */
add_filter( 'gform_column_input_content_8_23_3', 'set_column_esr_manufacturer', 10, 6 );
function set_column_esr_manufacturer( $input, $input_info, $field, $text, $value, $form_id ) {

	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;

}

/*
 * Equipment Service Request Form
 * Model
 */
add_filter( 'gform_column_input_content_8_23_4', 'set_column_esr_model', 10, 6 );
function set_column_esr_model( $input, $input_info, $field, $text, $value, $form_id ) {

	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;

}


/*
 * Equipment Service Request Form
 * Serial
 */
add_filter( 'gform_column_input_content_8_23_5', 'set_column_serial', 10, 6 );
function set_column_serial( $input, $input_info, $field, $text, $value, $form_id ) {

	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;

}

/*
 * Equipment Service Request Form
 * Location
 */
add_filter( 'gform_column_input_8_23_6', 'set_column_ram_location', 10, 5 );
function set_column_ram_location( $input_info, $field, $column, $value, $form_id ) {
	$choices = array(
		'To be shipped to Raeyco',
		'Customer Site'
	);

	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value, false )
	);
}


/*
 * Equipment Service Request Form
 * Serial
 */
add_filter( 'gform_column_input_content_8_23_7', 'set_column_customer_site', 10, 6 );
function set_column_customer_site( $input, $input_info, $field, $text, $value, $form_id ) {

	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;
}


/*
 * Equipment Service Request Form
 * Service type
 */
add_filter( 'gform_column_input_8_23_8', 'set_column_service_type', 10, 5 );
function set_column_service_type( $input_info, $field, $column, $value, $form_id ) {
	$choices = array(
		'Select Service type*',
		'Repair/Inspection',
		'Basic Calibration (Without Data)',
		'Gold Calibration (With Data)',
		'ISO17025 (see note below)',
		'Other'
	);

	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value )
	);
}


/*
 * Equipment Service Request Form
 * Other Service Type
 */
add_filter( 'gform_column_input_content_8_23_9', 'set_column_other_service_type', 10, 6 );
function set_column_other_service_type( $input, $input_info, $field, $text, $value, $form_id ) {

	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;
}

/*
 * Equipment Service Request Form
 * Service type
 */
add_filter( 'gform_column_input_8_23_10', 'set_column_calibration_interval', 10, 5 );
function set_column_calibration_interval( $input_info, $field, $column, $value, $form_id ) {
	$choices = array(
		'Select Calibration Interval*',
		'3 Months',
		'6 Months',
		'12 Months',
		'24 Months',
		'36 Months',
		'Other'
	);

	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value )
	);
}


/*
 * Equipment Service Request Form
 * Other Calibration Interval
 */
add_filter( 'gform_column_input_content_8_23_11', 'set_column_other_calibration_interval', 10, 6 );
function set_column_other_calibration_interval( $input, $input_info, $field, $text, $value, $form_id ) {

	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;
}


/*
 * Equipment Service Request Form
 * Additional Comments
 */
add_filter( 'gform_column_input_content_8_23_12', 'set_column_other_additional_comments', 10, 6 );
function set_column_other_additional_comments( $input, $input_info, $field, $text, $value, $form_id ) {

	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;
}

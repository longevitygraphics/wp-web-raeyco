<?php
function add_child_template_support() {
	/*add theme colors*/
	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => __( 'Primary', 'wp-theme-parent' ),
				'slug'  => 'primary',
				'color' => '#006480',
			),
			array(
				'name'  => __( 'Secondary', 'wp-theme-parent' ),
				'slug'  => 'secondary',
				'color' => '#D89B2B',
			),
			array(
				'name'  => __( 'Tertiary', 'wp-theme-parent' ),
				'slug'  => 'tertiary',
				'color' => '#5E5E5E',
			),
			array(
				'name'  => __( 'Body Text', 'wp-theme-parent' ),
				'slug'  => 'body-text',
				'color' => '#111111',
			),
			array(
				'name'  => __( 'Light Gray', 'wp-theme-parent' ),
				'slug'  => 'light-gray',
				'color' => '#FAFAFA',
			),
			array(
				'name'  => __( 'White', 'wp-theme-parent' ),
				'slug'  => 'white',
				'color' => '#ffffff',
			),
			array(
				'name'  => __( 'Dark', 'wp-theme-parent' ),
				'slug'  => 'dark',
				'color' => '#333333',
			),
		)
	);

	// add support for editor styles
	add_theme_support( 'editor-styles' );
}

add_action( 'after_setup_theme', 'add_child_template_support', 20 );

add_image_size( 'lg_woocommerce_product_category', 240, 170, false );
add_image_size( 'lg_woocommerce_product', 600, 425, false );

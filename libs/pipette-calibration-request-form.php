<?php


function get_select_choices( $choices, $selected, $firstIsEmptyOption = true ) {

	$options = array();
	foreach ( $choices as $k => $choice ) {
		if ( is_array( $choice ) ) {
			$option = $choice;
		} else {
			$option = array( 'text' => $choice, 'value' => $choice );
		}
		if ( $k == 0 and $firstIsEmptyOption ) {
			$option['value'] = "";
		}
		if ( $selected == $choice ) {
			$option['selected'] = 'selected';
		}
		$options[] = $option;
	}

	return $options;
}

/*
 * Pipette Info and Service Request
 * Level of Service
 */
add_filter( 'gform_column_input_5_12_4', 'set_column_level_of_service', 10, 5 );
function set_column_level_of_service( $input_info, $field, $column, $value, $form_id ) {
	//echo "<pre>";
	//var_dump( $input_info );
	//pr( $field );
	$choices = array(
		'Level of Service*',
		'Basic Inspection',
		'Calibration Gold',
		'Calibration Platinum',
		'ISO 17025'
	);

	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value )
	);
}

/*
 * Pipette Info and Service Request
 * Data Type
 */
add_filter( 'gform_column_input_5_12_5', 'set_column_data_type', 10, 5 );
function set_column_data_type( $input_info, $field, $column, $value, $form_id ) {
	//echo "<pre>";
	//var_dump( $input_info );
	//pr( $field );
	$choices = array(
		'Data Type*',
		'As Left',
		'As Found',
		'As Left & As Found'
	);

	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value )
	);
}


/*
 * Pipette Info and Service Request
 * Pipette type
 */
add_filter( 'gform_column_input_5_12_1', 'set_column_pipette_type', 10, 5 );
function set_column_pipette_type( $input_info, $field, $column, $value, $form_id ) {
	//echo "<pre>";
	//var_dump( $input_info );
	//pr( $field );
	$choices = array(
		'Pipette Type*',
		'Single Channel',
		'Multi Channel (8)',
		'Multi Channel (10)',
		'Multi Channel (12)',
		'Multi Channel (16)',
		'Repeater'
	);

	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value )
	);
}


/*
 * Pipette Info and Service Request
 * Number of channels per pipette
 */
add_filter( 'gform_column_input_5_12_2', 'set_column_number_of_channels', 10, 5 );
function set_column_number_of_channels( $input_info, $field, $column, $value, $form_id ) {
	//echo "<pre>";
	//var_dump( $input_info );
	//pr( $field );
	$choices = array(
		'Number of channels per pipette*',
		'All Channels',
		'Only 1 Channel (ch 1)',
		'Only Odd # channel (Ch 1,3,5..)'
	);

	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value )
	);
}


/*
 * Pipette Info and Service Request
 * Quantity
 */
add_filter( 'gform_column_input_content_5_12_3', 'set_column_quantity', 10, 6 );
function set_column_quantity( $input, $input_info, $field, $text, $value, $form_id ) {
	//echo "<pre>";
	//pr( $field );
	//pr( $text );
	//die;
	$input_field_name = 'input_' . $field->id . '[]';
	$tabindex         = GFCommon::get_tabindex();
	$new_input        = '<input type="text" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="' . $text . '" value="' . $value . '">';

	return $new_input;

}

/*
 * Pipette Info and Service Request
 * Service Intervals
 */
add_filter( 'gform_column_input_5_12_6', 'set_column_service_intervals', 10, 5 );
function set_column_service_intervals( $input_info, $field, $column, $value, $form_id ) {
	//echo "<pre>";
	//pr( $input_info );
	//pr( $field );
	//pr( $column );
	$choices = array(
		'Service Interval*',
		'Every 6 Months',
		'Every 12 Months',
		'Every 18 Months',
		'Every 24 Months',
		'Every 36 Months'
	);

	return array(
		'type'       => 'select',
		'isRequired' => 1,
		'choices'    => get_select_choices( $choices, $value )
	);
}


//get_all_form_fields(2);

function get_all_form_fields( $form_id ) {
	$form   = RGFormsModel::get_form_meta( $form_id );
	$fields = array();
	pr( $form );
	if ( is_array( $form["fields"] ) ) {
		foreach ( $form["fields"] as $field ) {
			if ( isset( $field["inputs"] ) && is_array( $field["inputs"] ) ) {

				foreach ( $field["inputs"] as $input ) {
					$fields[] = array( $input["id"], GFCommon::get_label( $field, $input["id"] ) );
				}
			} else if ( ! rgar( $field, 'displayOnly' ) ) {
				$fields[] = array( $field["id"], GFCommon::get_label( $field ) );
			}
		}
	}
	echo "<pre>";
	print_r( $fields );
	echo "</pre>";
	//die;

	//return $fields;
}

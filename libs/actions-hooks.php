<?php

function remove_woocommerce_before_shop_loop_hooks() {

	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

}

add_action( 'woocommerce_before_shop_loop', 'remove_woocommerce_before_shop_loop_hooks', 1 );

<?php
function pr( $data ) {
	echo "<pre>";
	print_r( $data );
	echo "</pre>";
}

function get_post_by_slug( $slug ) {
	$posts = get_posts( array(
		'name'           => $slug,
		'posts_per_page' => 1,
		'post_type'      => 'acf-field',
		'post_status'    => 'publish'
	) );

	if ( $posts ) {
		return $posts[0];
	}

	return 0;
}


/**
 * @param $lg_product_tab_keys
 */
function lg_wc_product_tabs( $lg_product_tab_keys ) {
	global $wp_query;
	//pr( $wp_query->query_vars );

	$active_tab_slug = '';
	$current_url     = '/' . $wp_query->query_vars['post_type'] . '/' . $wp_query->query_vars['name'];
	//get default tab
	foreach ( $lg_product_tab_keys as $value ):
		if ( have_rows( $value['name'] ) ):
			$active_tab_slug = $value['url_slug'];
			break;
		endif;
	endforeach;

	if ( isset( $wp_query->query_vars['tab'] ) and ! empty( $wp_query->query_vars['tab'] ) ) {
		$active_tab_slug = $wp_query->query_vars['tab'];
	}
	/*pr(get_fields());
	die;*/
//pr(acf_get_fields('group_5d1be3641d401'));
//pr( get_field_object( 'field_5d1be43675a05') );
	//$all_tabs = get_post_custom( 160 );
//pr($all_tabs);
	//die;
	if ( $lg_product_tab_keys ): ?>
		<div class="lg-product-tabs-wrap">
			<div class="lg-product-tabs-ul-wrap">
				<div class="lg-product-tabs-ul-mobile">
					<span></span>
					<a>
						<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" role="img"
						     focusable="false"><title>Menu</title>
							<path stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"
							      d="M4 7h22M4 15h22M4 23h22"></path>
						</svg>
					</a>
				</div>
				<ul class="nav nav-tabs" id="lg-product-tabs" role="tablist">

					<?php
					//loop through all tabs
					$tab_counter = 0;
					foreach ( $lg_product_tab_keys as $value ):
						$skip_row = true;
						// pr(have_rows($value['name']));
						if ( have_rows( $value['name'] ) ): $tab_counter ++;
							while ( have_rows( $value['key'] ) ) {
								the_row();
								$lg_row_fields = array_filter( get_row() );
								if ( $lg_row_fields and is_array( $lg_row_fields ) and count( $lg_row_fields ) ) {
									$skip_row = false;
									break;
								}
							}
							if ( $skip_row === false ): ?>
								<li class="nav-item">
									<a class="nav-link <?php echo $active_tab_slug === $value['url_slug'] ? 'active' : ''; ?>"
									   id="<?php echo $value['url_slug'] ?>-tab"
									   href="#<?php echo $value['url_slug'] ?>"
									   role="tab"
									   data-slug="<?php echo $value['url_slug']; ?>"
									   data-id="<?php echo get_the_ID(); ?>"
									><?php echo $value['label'] ?></a>
								</li>
							<?php endif; ?>

						<?php
						endif;
					endforeach; ?>
				</ul>
			</div>
		</div>
	<?php endif;

}


function lg_wc_flexible_layout( $lg_product_tab_keys ) {
	global $wp_query;
	//pr( $wp_query->query_vars );

	$active_tab_slug = '';
	$current_url     = '/' . $wp_query->query_vars['post_type'] . '/' . $wp_query->query_vars['name'];
	//get default tab
	foreach ( $lg_product_tab_keys as $value ):
		if ( have_rows( $value['name'] ) ):
			$active_tab_slug = $value['url_slug'];
			break;
		endif;
	endforeach;

	if ( isset( $wp_query->query_vars['tab'] ) and ! empty( $wp_query->query_vars['tab'] ) ) {
		$active_tab_slug = $wp_query->query_vars['tab'];
	}
//pr(acf_get_fields('group_5d1be3641d401'));
//pr( get_field_object( 'field_5d1be43675a05') );
	//$all_tabs = get_post_custom( 160 );
//pr($all_tabs);
	//pr( get_fields() );

	while ( have_rows( 'product_description_content' ) ): the_row();
		//pr( get_sub_field('flexible_content_block') );
		//get_template_part( '/components/acf-flexible-layout/layouts' );
	endwhile;
	//die;


	if ( $lg_product_tab_keys ): ?>
		<div class="lg-product-tabs-wrap">
			<div class="tab-content" id="lg-product-tabs-content">
				<?php
				$tab_counter = 0;
				foreach ( $lg_product_tab_keys as $value ):
					//pr($value);

					if ( have_rows( $value['name'] ) ): $tab_counter ++; ?>
						<div
							class="tab-pane <?php echo $active_tab_slug === $value['url_slug'] ? 'show active' : ''; ?>"
							id="<?php echo $value['url_slug'] ?>"
							role="tabpanel"
						><?php
							//on page load , get the content for selected tab
							//                            //if ($active_tab_slug and $active_tab_slug == $value['url_slug']) {
							//                            //echo $value['key'];
							while ( have_rows( $value['key'] ) ) : the_row();
								get_template_part( '/components/acf-flexible-layout/layouts' );
							endwhile;
							//}
							?></div>
					<?php
					else : // no layouts found
						?>

					<?php endif;
				endforeach; ?>
			</div>
		</div>
	<?php endif;

}


function render_tab_pane( $selected_tab_slug, $product_id ) {
	global $lg_product_tab_keys;
	//global $post;
	$post_object = get_post( $product_id, OBJECT );
	setup_postdata( $GLOBALS['post'] =& $post_object );
	set_current_screen();
	$tab_acf = array();
	if ( ! $product_id ) {
		return;
	}
	foreach ( $lg_product_tab_keys as $value ) {
		if ( $value['url_slug'] == $selected_tab_slug ) {
			$tab_acf = $value;
			break;
		}
	}


	if ( have_rows( $tab_acf['name'] ) ):
		while ( have_rows( $tab_acf['name'] ) ) : the_row();
			lg_wc_flexible_layout_switch( get_row_layout() );
		endwhile;
	else : // no layouts found

	endif;

	wp_reset_postdata();
}

// //REMOVE THIS
// add_action( 'wp_head', 'my_custom_styles', 100 );

// function my_custom_styles() {
// 	$test_css = '.lg-product-tabs-wrap {clear: both; margin-bottom: 50px;}';
// 	//echo "<style>$test_css</style>";
// }



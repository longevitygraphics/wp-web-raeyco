// Windows Ready Handler

(function ($) {
    $(document).ready(function () {
        //tabs click event
        $("#lg-product-tabs a").click(function (e) {
            e.preventDefault();
            var isMobile = $(".lg-product-tabs-ul-wrap").hasClass("is-mobile");
            var lgProductTabs = $("#lg-product-tabs");
            var productTabsMobileMenu = $(".lg-product-tabs-ul-mobile");

            if (isMobile) {
                productTabsMobileMenu.find("span").html($(this).html());
                lgProductTabs.hide();
            }

            if ($(this).hasClass("active")) {
                return true;
            }

            $("#lg-product-tabs a").removeClass("active");
            $(this).addClass("active");

            $("#lg-product-tabs-content .tab-pane").removeClass("show active");

            $($(this).attr("href")).addClass("show active");

            var tabContentOffset = 200;
            var $body = $("body");
            if ($body.hasClass("so-vantage-mobile-device")) {
                tabContentOffset = 52;
            }
            $("html, body").animate(
                {
                    scrollTop:
                        $($(this).attr("href")).offset().top - tabContentOffset,
                },
                300,
                "linear"
            );
        });

        //setup product tabs mobile menu
        var productTabsMobileMenu = $(".lg-product-tabs-ul-mobile");
        var activeProductTab = $("#lg-product-tabs .nav-link.active");
        productTabsMobileMenu.find("span").html(activeProductTab.html());

        setupTabsMobileMenu();

        $(".lg-product-tabs-ul-mobile a").click(function (e) {
            $("#lg-product-tabs").toggle();
        });

        /*$("#lg-product-tabs a").on('shown.bs.tab', function (e) {
            //debugger
            var currentTab = e.target;
            var tabLoaded = false;
            var tabSlug = $(currentTab).attr("data-slug");
            var productId = $(currentTab).attr("data-id");
            let url = $(currentTab).attr("href");
            var lgProductTabContent = $("lg-product-tabs-content");
            //check if tab is already loaded


            //get the tab content
            $.ajax({
                url: lg_ajax_obj.ajaxurl,
                data: {
                    'action': 'get_tab_content',
                    'tab': tabSlug,
                    'id': productId
                },
                success: function (data) {
                    console.log(data);
                    lgProductTabContent.append(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });

            window.history.pushState('object', '', url+ '/' + tabSlug);

            //stop browser from following the link
            e.preventDefault();
        });*/

        /**
         * covert the default dropdown to tiles
         */
        $(".variations select").each(function () {
            $(this).togglebutton();
        });
    });

    $(window).on("resize", function () {
        setupTabsMobileMenu();
    });

    function setupTabsMobileMenu() {
        var productTabsUlWrap = $(".lg-product-tabs-ul-wrap");
        var lgProductTabs = $("#lg-product-tabs");
        if (lgProductTabs.height() > 55) {
            productTabsUlWrap.addClass("is-mobile");
        } else {
            productTabsUlWrap.removeClass("is-mobile");
        }
    }

    // // Define the togglebutton plugin.
    // $.fn.togglebutton = function (opts) {
    //     // Apply the users options if exists.
    //     var settings = $.extend({}, $.fn.togglebutton.defaults, opts);

    //     // For each select element.
    //     this.each(function () {
    //         var self = $(this);
    //         var multiple = this.multiple;

    //         // Retrieve all options.
    //         var options = self.children("option");
    //         // Create an array of buttons with the value of select options.
    //         var buttons = options.map(function (index, opt) {
    //             var button = $(
    //                 "<button type='button' class='btn btn-default'></button>"
    //             )
    //                 .prop("value", opt.value)
    //                 .text(opt.text);

    //             // Add an `active` class if the option has been selected.
    //             if (opt.selected) button.addClass("active");

    //             // Return the button.
    //             return button[0];
    //         });

    //         // For each button, implement the click button removing and adding
    //         // `active` class to simulate the toggle effect. And also change the
    //         // select selected option.
    //         buttons.each(function (index, btn) {
    //             $(btn).click(function () {
    //                 // Retrieve all buttons siblings of the clicked one with an
    //                 // `active` class !
    //                 var activeBtn = $(btn).siblings(".active");
    //                 var total = [];

    //                 // Check if the clicked button has the class `active`.
    //                 // Add or remove it according to the check.
    //                 if ($(btn).hasClass("active")) {
    //                     //$(btn).removeClass("active");
    //                     return true;
    //                 } else {
    //                     $(btn).addClass("active");
    //                     options.val(btn.value).prop("selected", true);
    //                     total.push(btn.value);
    //                 }

    //                 // Remove all selected property on options.
    //                 self.children("option:selected").prop("selected", false);

    //                 // If the select allow multiple values, remove all active
    //                 // class to the other buttons (to keep only the last clicked
    //                 // button).
    //                 if (!multiple) {
    //                     activeBtn.removeClass("active");
    //                 }

    //                 // Push all active buttons value in an array.
    //                 activeBtn.each(function (index, btn) {
    //                     total.push(btn.value);
    //                 });

    //                 // Change selected options of the select.
    //                 self.val(btn.value).change();
    //             });
    //         });

    //         // Group all the buttons in a `div` element.
    //         var btnGroup = $("<div class='btn-group'>").append(buttons);
    //         // Include the buttons group after the select element.
    //         self.after(btnGroup);
    //         // Hide the display element.
    //         self.hide();
    //     });
    // };

    // // Set the defaults options of the plugin.
    // $.fn.togglebutton.defaults = {};
})(jQuery);

<div class="main-navigation container">
	<nav class="navbar navbar-expand-xl">
		<!-- Main Menu  -->
		<div class="navbar-collapse offcanvas-collapse" id="main-navbar" >
			<?php

			$mainMenu = array(
				// 'menu'              => 'menu-1',
				'theme_location' => 'top-nav',
				'depth'          => 4,
				'container'      => false,
				/*'container' => 'div',
				'container_class' => 'navbar-collapse offcanvas-collapse bg-primary',
				'container_id' => 'main-navbar',*/
				'menu_class'     => 'navbar-nav',
				'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
				'walker'         => new WP_Bootstrap_Navwalker()
			);
			wp_nav_menu( $mainMenu );

			?>
			<div class='pt-3 pb-5 d-xl-none mobile-nav-bottom'>
				<div><a href='#'>
					<i class="far fa-user"></i> Labforce Login</a>
				</div>
				<div class='header-hours mt-3'>
					<i class="far fa-clock"></i> Mon-Fri 8am-5pm | Sat-Sun: Closed
				</div>
			</div>
		</div>
	</nav>
</div>

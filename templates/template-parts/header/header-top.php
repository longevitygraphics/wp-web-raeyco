<div class='header-top-desktop px-3 py-2 justify-content-between'>

	<div class="d-flex flex-column justify-content-between">
	<?php echo do_shortcode( "[lg-social-media]" ) ?>
	<div class="lg-search-form"><?php get_search_form(); ?></div>
	</div>

	<div class='site-branding d-flex flex-column align-items-center'>
		<a href="/" class='logo mb-1'>
		<?php echo wp_get_attachment_image(3810) ?>
		</a>
		<p><?php bloginfo( 'description' ); ?></p>
	</div>
	<div class='header-top-right d-flex flex-column justify-content-between align-items-end'>
		<div><a class='text-primary' href='https://raeyco.force.com/login'><i class="far fa-user"></i> Labforce
				Login</a></div>
		<div>
			<a class='btn btn-secondary mr-3' href="tel: <?php echo do_shortcode( '[lg-phone-main]' ); ?>"><i
					class="fas fa-phone-alt"></i> <?php echo do_shortcode( '[lg-phone-main]' ); ?></a>
			<a class='btn btn-primary' href="mailto: <?php echo do_shortcode( '[lg-email]' ); ?>"><i
					class="far fa-envelope"></i> Email Us</a>
		</div>
	</div>
</div>
<div class='header-top-mobile d-flex d-xl-none justify-content-between align-items-center p-2'>
	<div>
		<button class="mobile-toggle hamburger hamburger--collapse" type="button">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
		</button>
	</div>
	<div class='site-branding d-flex flex-column align-items-center'>
		<a href="/" class='logo'>
			<?php echo wp_get_attachment_image(3810) ?>
		</a>
	</div>
	<div class='header-top-right'>
		<a class='mr-2' href="tel: <?php echo do_shortcode( '[lg-phone-main]' ); ?>"><i
				class="fas fa-phone-alt"></i></a>
		<a href="mailto: <?php echo do_shortcode( '[lg-email]' ); ?>"><i class="far fa-envelope"></i></a>
	</div>
</div>

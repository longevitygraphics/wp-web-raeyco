// Window Scroll Handler

(function($) {
	var position = document.documentElement.scrollTop;
    $(window).on('scroll', function(){
		var scroll = document.documentElement.scrollTop;
		if (scroll > position) {
			// $('.header-main').addClass('small-header');
			if ($(window).width() > 1200) {
				$('.header-top-desktop').slideUp();
			}
		} else {
			// $('.header-main').removeClass('small-header');
			if ($(window).width() > 1200) {
				$('.header-top-desktop').slideDown();
			}
		}
		position = scroll;
    })

}(jQuery));
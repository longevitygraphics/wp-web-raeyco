// Windows Ready Handler

(function ($) {
    $(document).ready(function () {
        var $hamburger = $(".hamburger");
        $hamburger.on("click", function (e) {
            $hamburger.toggleClass("is-active");
            $(".main-navigation .offcanvas-collapse").toggleClass("show");
        });

        // function handleWindow() {
        //     var body = document.querySelector("body");

        //     if (window.innerWidth > body.clientWidth + 5) {
        //         //body.classList.add("has-scrollbar");
        //         body.setAttribute(
        //             "style",
        //             "--scroll-bar: " +
        //                 (window.innerWidth - body.clientWidth) +
        //                 "px"
        //         );
        //     } else {
        //         //body.classList.remove('has-scrollbar');
        //     }
        // }

        //handleWindow();

        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(
                /[?&]+([^=&]+)=([^&]*)/gi,
                function (m, key, value) {
                    vars[key] = value;
                }
            );
            return vars;
        }

        // slide to a section
        var para = getUrlVars()["slideto"];
        var element = $("#" + para);
        var settings = {
            duration: 2 * 1000,
            offset: -80,
        };
        if (para && element[0]) {
            var scrollTo = element;
            KCollection.headerScrollTo(scrollTo, settings, function () {
                console.log("here now");
            });
        }
    });

    /**
     * WooCommerce Product Category Accordion jQuery Menu
     * @link https://wpbeaches.com/woocommerce-accordion-style-expand-collapse-product-category-menu/
     */

    if ($("ul.product-categories").length > 0) {
        // Set variables
        // pC = Parent Category
        // fpC = First Parent Category
        // cC = Current Category
        // cCp = Currents Category's Parent

        var pC = $(".product-categories li.cat-parent"),
            fpC = $(".product-categories li.cat-parent:first-child"), // Start this one open
            cC = $(".product-categories li.current-cat"),
            cCp = $(".product-categories li.current-cat-parent");

        pC.prepend(
            '<span class="toggle"><i class="far fa-minus-square fa-plus-square"></i></span>'
        );
        pC.parent("ul").addClass("has-toggle");
        pC.children("ul").hide();

        if (pC.hasClass("current-cat-parent")) {
            cCp.addClass("open");
            cCp.children("ul").show();
            cCp.children().children("i.far").removeClass("fa-plus-square");
        } else if (pC.hasClass("current-cat")) {
            cC.addClass("open");
            cC.children("ul").show();
            cC.children().children("i.far").removeClass("fa-plus-square");
        } else {
            fpC.addClass("open");
            fpC.children("ul").show();
            fpC.children().children("i.far").removeClass("fa-plus-square");
        }

        $(".has-toggle span.toggle").on("click", function () {
            $t = $(this);
            if ($t.parent().hasClass("open")) {
                $t.parent().removeClass("open");
                $t.parent().children("ul").slideUp();
                $t.children("i.far").addClass("fa-plus-square");
            } else {
                $t.parent().parent().find("ul.children").slideUp();
                $t.parent().parent().find("li.cat-parent").removeClass("open");
                $t.parent()
                    .parent()
                    .find("li.cat-parent")
                    .children()
                    .children("i.far")
                    .addClass("fa-plus-square");

                $t.parent().addClass("open");
                $t.parent().children("ul").slideDown();
                $t.children("i.far").removeClass("fa-plus-square");
            }
        });

        // Link the count number
        $(".count").css("cursor", "pointer");
        $(".count").on("click", function (event) {
            $(this).prev("a")[0].click();
        });
    }

    if ($(".swiper-container.variant-table-swiper").length > 0) {
        let swiperElem = new Swiper(".swiper-container.variant-table-swiper", {
            speed: 400,
            slidesPerView: 1,
            allowTouchMove: true,
            spaceBetween: 0,
            loop: true,
            breakpoints: {
                // when window width is >= 376px
                376: {
                    slidesPerView: 2,
                    allowTouchMove: true,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 3,
                    allowTouchMove: true,
                    loop: false,
                },
                // when window width is >= 992px
                992: {
                    slidesPerView: 4,
                    loop: false,
                },
            },
            navigation: {
                nextEl: ".swiper-button-next-item",
                prevEl: ".swiper-button-prev-item",
            },
        });

        $(".swiper-button-next").on("click", function () {
            swiperElem.slideNext();
        });
        $(".swiper-button-prev").on("click", function () {
            swiperElem.slidePrev();
        });
    }

    if ($(".swiper-container.home-slider").length > 0) {
        let swiperElem = new Swiper(".swiper-container.home-slider", {
            speed: 400,
            slidesPerView: 1,
            allowTouchMove: false,
            loop: true,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });

        console.log(swiperElem);
        window.testing = swiperElem;
        $(".swiper-button-next").on("click", function () {
            swiperElem.slideNext();
            stopProp;
        });
        $(".swiper-button-prev").on("click", function () {
            swiperElem.slidePrev();
        });
    }
})(jQuery);

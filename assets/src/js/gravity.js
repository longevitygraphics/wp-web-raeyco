(function ($) {

  jQuery(document).on('gform_post_render', function () {

    if ($(".pipette-service-request").length) {


      gform.addAction('gform_list_post_item_add', function (item, container) {
        //console.log(item, container);
        const levelOfServiceSelectEl = $(".pipette-service-request .gfield_list_12_cell4 select");
        const pipetteTypeSelectEl = $(".pipette-service-request .gfield_list_12_cell1 select");
        //bind the events
        levelOfServiceSelectEl.change(function () {
          levelOfServiceOnChange($(this));
        });
        pipetteTypeSelectEl.change(function () {
          pipetteTypeOnChange($(this));
        });
      });


      const levelOfServiceSelectEl = $(".pipette-service-request .gfield_list_12_cell4 select");
      const pipetteTypeSelectEl = $(".pipette-service-request .gfield_list_12_cell1 select");

      //bind the events
      levelOfServiceSelectEl.change(function () {
        levelOfServiceOnChange($(this));
      });
      pipetteTypeSelectEl.change(function () {
        pipetteTypeOnChange($(this));
      });

      //this is needed when form is submitted with validation errors
      levelOfServiceOnChange(levelOfServiceSelectEl);
    }

    if ($(".equipment-service-request").length) {

      gform.addAction('gform_list_post_item_add', function (item, container) {
        //debugger
        const haveRAMSelectEl = $(item).find(".gfield_list_23_cell1 select");
        const locationSelectEl = $(item).find(".gfield_list_23_cell6 select");
        const serviceTypeSelectEl = $(item).find(".gfield_list_23_cell8 select");
        const calibrationIntervalSelectEl = $(item).find(".gfield_list_23_cell10 select");

        haveRAMSelectEl.change(function () {
          haveRAMOnChange($(this));
        });
        locationSelectEl.change(function () {
          locationOnchange($(this));
        });
        serviceTypeSelectEl.change(function () {
          serviceTypeOnChange($(this));
        });
        calibrationIntervalSelectEl.change(function () {
          calibrationIntervalOnchange($(this));
        });
        jQuery(haveRAMSelectEl).prop("selectedIndex", 0).change();
        jQuery(locationSelectEl).prop("selectedIndex", 0).change();
        jQuery(serviceTypeSelectEl).prop("selectedIndex", 0).change();
        jQuery(calibrationIntervalSelectEl).prop("selectedIndex", 0).change();
      });

      const haveRAMSelectEl = $(".gfield_list_23_cell1 select");
      const locationSelectEl = $(".gfield_list_23_cell6 select");
      const serviceTypeSelectEl = $(".gfield_list_23_cell8 select");
      const calibrationIntervalSelectEl = $(".gfield_list_23_cell10 select");

      haveRAMSelectEl.change(function () {
        haveRAMOnChange($(this));
      });
      locationSelectEl.change(function () {
        locationOnchange($(this));
      });
      serviceTypeSelectEl.change(function () {
        serviceTypeOnChange($(this));
      });
      calibrationIntervalSelectEl.change(function () {
        calibrationIntervalOnchange($(this));
      });
    }


    //have RAM#
    function haveRAMOnChange(haveRAMSelectEl) {
      const haveRAM = $(haveRAMSelectEl).val();
      const tr = $(haveRAMSelectEl).parents("tr");
      if (haveRAM === "yes") {
        $(tr).find(".gfield_list_23_cell2").removeClass('d-none').addClass('d-block');
        $(tr).find(".gfield_list_23_cell3").removeClass('d-block').addClass('d-none');
        $(tr).find(".gfield_list_23_cell4").removeClass('d-block').addClass('d-none');
        $(tr).find(".gfield_list_23_cell5").removeClass('d-block').addClass('d-none');
        $(tr).find(".gfield_list_23_cell6").removeClass('d-block').addClass('d-none');
      } else {
        $(tr).find(".gfield_list_23_cell2").removeClass('d-block').addClass('d-none');
        $(tr).find(".gfield_list_23_cell3").removeClass('d-none').addClass('d-block');
        $(tr).find(".gfield_list_23_cell4").removeClass('d-none').addClass('d-block');
        $(tr).find(".gfield_list_23_cell5").removeClass('d-none').addClass('d-block');
        $(tr).find(".gfield_list_23_cell6").removeClass('d-none').addClass('d-block');
      }
    }

    //Location
    function locationOnchange(locationSelectEl) {
      const location = $(locationSelectEl).val();
      const tr = $(locationSelectEl).parents("tr");
      if (location === 'Customer Site') {
        $(tr).find(".gfield_list_23_cell7").removeClass('d-none').addClass('d-block');
      } else {
        $(tr).find(".gfield_list_23_cell7").removeClass('d-block').addClass('d-none');
      }
    }

    //service type
    function serviceTypeOnChange(serviceTypeSelectEl) {
      const serviceType = $(serviceTypeSelectEl).val();
      const tr = $(serviceTypeSelectEl).parents("tr");
      if (serviceType === 'Repair/Inspection' || serviceType === 'Other') {
        $(tr).find(".gfield_list_23_cell10").removeClass('d-block').addClass('d-none');
      } else {
        $(tr).find(".gfield_list_23_cell10").removeClass('d-none').addClass('d-block');
      }
      if (serviceType === 'Other') {
        $(tr).find(".gfield_list_23_cell9").removeClass('d-none').addClass('d-block');
      } else {
        $(tr).find(".gfield_list_23_cell9").removeClass('d-block').addClass('d-none');
      }
    }

    //calibration interval
    function calibrationIntervalOnchange(calibrationIntervalSelectEl) {
      const serviceType = $(calibrationIntervalSelectEl).val();
      const tr = $(calibrationIntervalSelectEl).parents("tr");
      if (serviceType === 'Other') {
        $(tr).find(".gfield_list_23_cell11").removeClass('d-none').addClass('d-block');
      } else {
        $(tr).find(".gfield_list_23_cell11").removeClass('d-block').addClass('d-none');
      }
    }


    function levelOfServiceOnChange(levelOfServiceSelectEl) {
      const levelOfService = levelOfServiceSelectEl.val();
      const dataTypeSelectEl = levelOfServiceSelectEl.closest("tr.gfield_list_group").find(".gfield_list_12_cell5 select");
      const asLeftOptionEl = dataTypeSelectEl.find("option[value='As Left']");
      const asFoundOptionEl = dataTypeSelectEl.find("option[value='As Found']");
      const asLeftAndAsFoundOptionEl = dataTypeSelectEl.find("option[value='As Left & As Found']");

      if (dataTypeSelectEl.prev().length) {
        dataTypeSelectEl.prev().remove();
      }

      dataTypeSelectEl.prop("disabled", false);
      //toggleOptionValues(dataTypeSelectEl, 'enabled');
      if (levelOfService === "Basic Inspection") {
        dataTypeSelectEl.val("");
        dataTypeSelectEl.parent().prepend('<input type="hidden" name="' + dataTypeSelectEl.attr('name') + '" value="NA"/>');
        dataTypeSelectEl.prop("disabled", true);
        //toggleOptionValues(dataTypeSelectEl, 'disabled');
      } else if (levelOfService === "Calibration Gold") {
        asLeftOptionEl.attr('disabled', false);
        asFoundOptionEl.attr('disabled', true);
        asLeftAndAsFoundOptionEl.attr('disabled', true);
      } else if (levelOfService === "Calibration Platinum") {
        asLeftOptionEl.attr('disabled', true);
        asFoundOptionEl.attr('disabled', true);
        asLeftAndAsFoundOptionEl.attr('disabled', false);
      } else if (levelOfService === "ISO 17025") {
        asLeftOptionEl.attr('disabled', false);
        asFoundOptionEl.attr('disabled', true);
        asLeftAndAsFoundOptionEl.attr('disabled', false);
      }
    }

    function pipetteTypeOnChange(pipetteTypeSelectEl) {
      const pipetteType = pipetteTypeSelectEl.val();
      const numberOfChannelsSelectEl = pipetteTypeSelectEl.closest("tr.gfield_list_group").find(".gfield_list_12_cell2 select");
      numberOfChannelsSelectEl.find('option').attr('disabled', false);
      if (numberOfChannelsSelectEl.prev().length) {
        numberOfChannelsSelectEl.prev().remove();
      }

      numberOfChannelsSelectEl.prop('disabled', false);
      //toggleOptionValues(numberOfChannelsSelectEl, 'enabled');
      if (pipetteType === 'Single Channel' || pipetteType === 'Repeater') {
        numberOfChannelsSelectEl.val('');
        numberOfChannelsSelectEl.parent().prepend('<input type="hidden" name="' + numberOfChannelsSelectEl.attr('name') + '" value="NA"/>');
        numberOfChannelsSelectEl.prop('disabled', true);
        //toggleOptionValues(numberOfChannelsSelectEl, 'disabled');
      } else {
        numberOfChannelsSelectEl.find('option').attr('disabled', true);
        numberOfChannelsSelectEl.find('option:first-child').attr('disabled', false);
        numberOfChannelsSelectEl.find("option[value='All Channels']").attr('disabled', false);
        numberOfChannelsSelectEl.find("option[value='Only 1 Channel (ch 1)']").attr('disabled', false);
        numberOfChannelsSelectEl.find("option[value='Only Odd # channel (Ch 1,3,5..)']").attr('disabled', false);
      }
    }

    function toggleOptionValues(el, state) {
      if (state === 'disabled') {
        el.find('option').each(function () {
          if (jQuery(this).attr("value") != "") {
            jQuery(this).prop('disabled', true);
          }
        });
      } else {
        el.find('option').each(function () {
          jQuery(this).prop('disabled', false);
        });
      }
    }
  });

}(jQuery));

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/entry.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/entry.js":
/*!*************************!*\
  !*** ./assets/entry.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./src/js/script */ "./assets/src/js/script.js");

/***/ }),

/***/ "./assets/src/js/gravity.js":
/*!**********************************!*\
  !*** ./assets/src/js/gravity.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  jQuery(document).on('gform_post_render', function () {
    if ($(".pipette-service-request").length) {
      gform.addAction('gform_list_post_item_add', function (item, container) {
        //console.log(item, container);
        var levelOfServiceSelectEl = $(".pipette-service-request .gfield_list_12_cell4 select");
        var pipetteTypeSelectEl = $(".pipette-service-request .gfield_list_12_cell1 select"); //bind the events

        levelOfServiceSelectEl.change(function () {
          levelOfServiceOnChange($(this));
        });
        pipetteTypeSelectEl.change(function () {
          pipetteTypeOnChange($(this));
        });
      });
      var levelOfServiceSelectEl = $(".pipette-service-request .gfield_list_12_cell4 select");
      var pipetteTypeSelectEl = $(".pipette-service-request .gfield_list_12_cell1 select"); //bind the events

      levelOfServiceSelectEl.change(function () {
        levelOfServiceOnChange($(this));
      });
      pipetteTypeSelectEl.change(function () {
        pipetteTypeOnChange($(this));
      }); //this is needed when form is submitted with validation errors

      levelOfServiceOnChange(levelOfServiceSelectEl);
    }

    if ($(".equipment-service-request").length) {
      gform.addAction('gform_list_post_item_add', function (item, container) {
        //debugger
        var haveRAMSelectEl = $(item).find(".gfield_list_23_cell1 select");
        var locationSelectEl = $(item).find(".gfield_list_23_cell6 select");
        var serviceTypeSelectEl = $(item).find(".gfield_list_23_cell8 select");
        var calibrationIntervalSelectEl = $(item).find(".gfield_list_23_cell10 select");
        haveRAMSelectEl.change(function () {
          haveRAMOnChange($(this));
        });
        locationSelectEl.change(function () {
          locationOnchange($(this));
        });
        serviceTypeSelectEl.change(function () {
          serviceTypeOnChange($(this));
        });
        calibrationIntervalSelectEl.change(function () {
          calibrationIntervalOnchange($(this));
        });
        jQuery(haveRAMSelectEl).prop("selectedIndex", 0).change();
        jQuery(locationSelectEl).prop("selectedIndex", 0).change();
        jQuery(serviceTypeSelectEl).prop("selectedIndex", 0).change();
        jQuery(calibrationIntervalSelectEl).prop("selectedIndex", 0).change();
      });
      var haveRAMSelectEl = $(".gfield_list_23_cell1 select");
      var locationSelectEl = $(".gfield_list_23_cell6 select");
      var serviceTypeSelectEl = $(".gfield_list_23_cell8 select");
      var calibrationIntervalSelectEl = $(".gfield_list_23_cell10 select");
      haveRAMSelectEl.change(function () {
        haveRAMOnChange($(this));
      });
      locationSelectEl.change(function () {
        locationOnchange($(this));
      });
      serviceTypeSelectEl.change(function () {
        serviceTypeOnChange($(this));
      });
      calibrationIntervalSelectEl.change(function () {
        calibrationIntervalOnchange($(this));
      });
    } //have RAM#


    function haveRAMOnChange(haveRAMSelectEl) {
      var haveRAM = $(haveRAMSelectEl).val();
      var tr = $(haveRAMSelectEl).parents("tr");

      if (haveRAM === "yes") {
        $(tr).find(".gfield_list_23_cell2").removeClass('d-none').addClass('d-block');
        $(tr).find(".gfield_list_23_cell3").removeClass('d-block').addClass('d-none');
        $(tr).find(".gfield_list_23_cell4").removeClass('d-block').addClass('d-none');
        $(tr).find(".gfield_list_23_cell5").removeClass('d-block').addClass('d-none');
        $(tr).find(".gfield_list_23_cell6").removeClass('d-block').addClass('d-none');
      } else {
        $(tr).find(".gfield_list_23_cell2").removeClass('d-block').addClass('d-none');
        $(tr).find(".gfield_list_23_cell3").removeClass('d-none').addClass('d-block');
        $(tr).find(".gfield_list_23_cell4").removeClass('d-none').addClass('d-block');
        $(tr).find(".gfield_list_23_cell5").removeClass('d-none').addClass('d-block');
        $(tr).find(".gfield_list_23_cell6").removeClass('d-none').addClass('d-block');
      }
    } //Location


    function locationOnchange(locationSelectEl) {
      var location = $(locationSelectEl).val();
      var tr = $(locationSelectEl).parents("tr");

      if (location === 'Customer Site') {
        $(tr).find(".gfield_list_23_cell7").removeClass('d-none').addClass('d-block');
      } else {
        $(tr).find(".gfield_list_23_cell7").removeClass('d-block').addClass('d-none');
      }
    } //service type


    function serviceTypeOnChange(serviceTypeSelectEl) {
      var serviceType = $(serviceTypeSelectEl).val();
      var tr = $(serviceTypeSelectEl).parents("tr");

      if (serviceType === 'Repair/Inspection' || serviceType === 'Other') {
        $(tr).find(".gfield_list_23_cell10").removeClass('d-block').addClass('d-none');
      } else {
        $(tr).find(".gfield_list_23_cell10").removeClass('d-none').addClass('d-block');
      }

      if (serviceType === 'Other') {
        $(tr).find(".gfield_list_23_cell9").removeClass('d-none').addClass('d-block');
      } else {
        $(tr).find(".gfield_list_23_cell9").removeClass('d-block').addClass('d-none');
      }
    } //calibration interval


    function calibrationIntervalOnchange(calibrationIntervalSelectEl) {
      var serviceType = $(calibrationIntervalSelectEl).val();
      var tr = $(calibrationIntervalSelectEl).parents("tr");

      if (serviceType === 'Other') {
        $(tr).find(".gfield_list_23_cell11").removeClass('d-none').addClass('d-block');
      } else {
        $(tr).find(".gfield_list_23_cell11").removeClass('d-block').addClass('d-none');
      }
    }

    function levelOfServiceOnChange(levelOfServiceSelectEl) {
      var levelOfService = levelOfServiceSelectEl.val();
      var dataTypeSelectEl = levelOfServiceSelectEl.closest("tr.gfield_list_group").find(".gfield_list_12_cell5 select");
      var asLeftOptionEl = dataTypeSelectEl.find("option[value='As Left']");
      var asFoundOptionEl = dataTypeSelectEl.find("option[value='As Found']");
      var asLeftAndAsFoundOptionEl = dataTypeSelectEl.find("option[value='As Left & As Found']");

      if (dataTypeSelectEl.prev().length) {
        dataTypeSelectEl.prev().remove();
      }

      dataTypeSelectEl.prop("disabled", false); //toggleOptionValues(dataTypeSelectEl, 'enabled');

      if (levelOfService === "Basic Inspection") {
        dataTypeSelectEl.val("");
        dataTypeSelectEl.parent().prepend('<input type="hidden" name="' + dataTypeSelectEl.attr('name') + '" value="NA"/>');
        dataTypeSelectEl.prop("disabled", true); //toggleOptionValues(dataTypeSelectEl, 'disabled');
      } else if (levelOfService === "Calibration Gold") {
        asLeftOptionEl.attr('disabled', false);
        asFoundOptionEl.attr('disabled', true);
        asLeftAndAsFoundOptionEl.attr('disabled', true);
      } else if (levelOfService === "Calibration Platinum") {
        asLeftOptionEl.attr('disabled', true);
        asFoundOptionEl.attr('disabled', true);
        asLeftAndAsFoundOptionEl.attr('disabled', false);
      } else if (levelOfService === "ISO 17025") {
        asLeftOptionEl.attr('disabled', false);
        asFoundOptionEl.attr('disabled', true);
        asLeftAndAsFoundOptionEl.attr('disabled', false);
      }
    }

    function pipetteTypeOnChange(pipetteTypeSelectEl) {
      var pipetteType = pipetteTypeSelectEl.val();
      var numberOfChannelsSelectEl = pipetteTypeSelectEl.closest("tr.gfield_list_group").find(".gfield_list_12_cell2 select");
      numberOfChannelsSelectEl.find('option').attr('disabled', false);

      if (numberOfChannelsSelectEl.prev().length) {
        numberOfChannelsSelectEl.prev().remove();
      }

      numberOfChannelsSelectEl.prop('disabled', false); //toggleOptionValues(numberOfChannelsSelectEl, 'enabled');

      if (pipetteType === 'Single Channel' || pipetteType === 'Repeater') {
        numberOfChannelsSelectEl.val('');
        numberOfChannelsSelectEl.parent().prepend('<input type="hidden" name="' + numberOfChannelsSelectEl.attr('name') + '" value="NA"/>');
        numberOfChannelsSelectEl.prop('disabled', true); //toggleOptionValues(numberOfChannelsSelectEl, 'disabled');
      } else {
        numberOfChannelsSelectEl.find('option').attr('disabled', true);
        numberOfChannelsSelectEl.find('option:first-child').attr('disabled', false);
        numberOfChannelsSelectEl.find("option[value='All Channels']").attr('disabled', false);
        numberOfChannelsSelectEl.find("option[value='Only 1 Channel (ch 1)']").attr('disabled', false);
        numberOfChannelsSelectEl.find("option[value='Only Odd # channel (Ch 1,3,5..)']").attr('disabled', false);
      }
    }

    function toggleOptionValues(el, state) {
      if (state === 'disabled') {
        el.find('option').each(function () {
          if (jQuery(this).attr("value") != "") {
            jQuery(this).prop('disabled', true);
          }
        });
      } else {
        el.find('option').each(function () {
          jQuery(this).prop('disabled', false);
        });
      }
    }
  });
})(jQuery);

/***/ }),

/***/ "./assets/src/js/script.js":
/*!*********************************!*\
  !*** ./assets/src/js/script.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sass_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../sass/style.scss */ "./assets/src/sass/style.scss");
/* harmony import */ var _sass_style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_sass_style_scss__WEBPACK_IMPORTED_MODULE_0__);


__webpack_require__(/*! ./window-event/ready.js */ "./assets/src/js/window-event/ready.js");

__webpack_require__(/*! ./window-event/load.js */ "./assets/src/js/window-event/load.js");

__webpack_require__(/*! ./window-event/resize.js */ "./assets/src/js/window-event/resize.js");

__webpack_require__(/*! ./window-event/scroll.js */ "./assets/src/js/window-event/scroll.js");

__webpack_require__(/*! ./gravity */ "./assets/src/js/gravity.js");

/***/ }),

/***/ "./assets/src/js/window-event/load.js":
/*!********************************************!*\
  !*** ./assets/src/js/window-event/load.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

// Windows Load Handler
(function ($) {
  $(window).on("load", function () {//sticky_header_initialize();
  });
})(jQuery);

var cls = 0;
new PerformanceObserver(function (entryList) {
  var _iterator = _createForOfIteratorHelper(entryList.getEntries()),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var entry = _step.value;

      if (!entry.hadRecentInput) {
        cls += entry.value;
        console.log("Current CLS value:", cls, entry);

        if (cls >= 0.1) {
          console.log("CLS FAILING");
        }
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
}).observe({
  type: "layout-shift",
  buffered: true
});

/***/ }),

/***/ "./assets/src/js/window-event/ready.js":
/*!*********************************************!*\
  !*** ./assets/src/js/window-event/ready.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Windows Ready Handler
(function ($) {
  $(document).ready(function () {
    var $hamburger = $(".hamburger");
    $hamburger.on("click", function (e) {
      $hamburger.toggleClass("is-active");
      $(".main-navigation .offcanvas-collapse").toggleClass("show");
    }); // function handleWindow() {
    //     var body = document.querySelector("body");
    //     if (window.innerWidth > body.clientWidth + 5) {
    //         //body.classList.add("has-scrollbar");
    //         body.setAttribute(
    //             "style",
    //             "--scroll-bar: " +
    //                 (window.innerWidth - body.clientWidth) +
    //                 "px"
    //         );
    //     } else {
    //         //body.classList.remove('has-scrollbar');
    //     }
    // }
    //handleWindow();

    function getUrlVars() {
      var vars = {};
      var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
      });
      return vars;
    } // slide to a section


    var para = getUrlVars()["slideto"];
    var element = $("#" + para);
    var settings = {
      duration: 2 * 1000,
      offset: -80
    };

    if (para && element[0]) {
      var scrollTo = element;
      KCollection.headerScrollTo(scrollTo, settings, function () {
        console.log("here now");
      });
    }
  });
  /**
   * WooCommerce Product Category Accordion jQuery Menu
   * @link https://wpbeaches.com/woocommerce-accordion-style-expand-collapse-product-category-menu/
   */

  if ($("ul.product-categories").length > 0) {
    // Set variables
    // pC = Parent Category
    // fpC = First Parent Category
    // cC = Current Category
    // cCp = Currents Category's Parent
    var pC = $(".product-categories li.cat-parent"),
        fpC = $(".product-categories li.cat-parent:first-child"),
        // Start this one open
    cC = $(".product-categories li.current-cat"),
        cCp = $(".product-categories li.current-cat-parent");
    pC.prepend('<span class="toggle"><i class="far fa-minus-square fa-plus-square"></i></span>');
    pC.parent("ul").addClass("has-toggle");
    pC.children("ul").hide();

    if (pC.hasClass("current-cat-parent")) {
      cCp.addClass("open");
      cCp.children("ul").show();
      cCp.children().children("i.far").removeClass("fa-plus-square");
    } else if (pC.hasClass("current-cat")) {
      cC.addClass("open");
      cC.children("ul").show();
      cC.children().children("i.far").removeClass("fa-plus-square");
    } else {
      fpC.addClass("open");
      fpC.children("ul").show();
      fpC.children().children("i.far").removeClass("fa-plus-square");
    }

    $(".has-toggle span.toggle").on("click", function () {
      $t = $(this);

      if ($t.parent().hasClass("open")) {
        $t.parent().removeClass("open");
        $t.parent().children("ul").slideUp();
        $t.children("i.far").addClass("fa-plus-square");
      } else {
        $t.parent().parent().find("ul.children").slideUp();
        $t.parent().parent().find("li.cat-parent").removeClass("open");
        $t.parent().parent().find("li.cat-parent").children().children("i.far").addClass("fa-plus-square");
        $t.parent().addClass("open");
        $t.parent().children("ul").slideDown();
        $t.children("i.far").removeClass("fa-plus-square");
      }
    }); // Link the count number

    $(".count").css("cursor", "pointer");
    $(".count").on("click", function (event) {
      $(this).prev("a")[0].click();
    });
  }

  if ($(".swiper-container.variant-table-swiper").length > 0) {
    var swiperElem = new Swiper(".swiper-container.variant-table-swiper", {
      speed: 400,
      slidesPerView: 1,
      allowTouchMove: true,
      spaceBetween: 0,
      loop: true,
      breakpoints: {
        // when window width is >= 376px
        376: {
          slidesPerView: 2,
          allowTouchMove: true
        },
        // when window width is >= 768px
        768: {
          slidesPerView: 3,
          allowTouchMove: true,
          loop: false
        },
        // when window width is >= 992px
        992: {
          slidesPerView: 4,
          loop: false
        }
      },
      navigation: {
        nextEl: ".swiper-button-next-item",
        prevEl: ".swiper-button-prev-item"
      }
    });
    $(".swiper-button-next").on("click", function () {
      swiperElem.slideNext();
    });
    $(".swiper-button-prev").on("click", function () {
      swiperElem.slidePrev();
    });
  }

  if ($(".swiper-container.home-slider").length > 0) {
    var _swiperElem = new Swiper(".swiper-container.home-slider", {
      speed: 400,
      slidesPerView: 1,
      allowTouchMove: false,
      loop: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      }
    });

    console.log(_swiperElem);
    window.testing = _swiperElem;
    $(".swiper-button-next").on("click", function () {
      _swiperElem.slideNext();

      stopProp;
    });
    $(".swiper-button-prev").on("click", function () {
      _swiperElem.slidePrev();
    });
  }
})(jQuery);

/***/ }),

/***/ "./assets/src/js/window-event/resize.js":
/*!**********************************************!*\
  !*** ./assets/src/js/window-event/resize.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Window Resize Handler
(function ($) {
  $(window).on('resize', function () {});
})(jQuery);

/***/ }),

/***/ "./assets/src/js/window-event/scroll.js":
/*!**********************************************!*\
  !*** ./assets/src/js/window-event/scroll.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Window Scroll Handler
(function ($) {
  var position = document.documentElement.scrollTop;
  $(window).on('scroll', function () {
    var scroll = document.documentElement.scrollTop;

    if (scroll > position) {
      // $('.header-main').addClass('small-header');
      if ($(window).width() > 1200) {
        $('.header-top-desktop').slideUp();
      }
    } else {
      // $('.header-main').removeClass('small-header');
      if ($(window).width() > 1200) {
        $('.header-top-desktop').slideDown();
      }
    }

    position = scroll;
  });
})(jQuery);

/***/ }),

/***/ "./assets/src/sass/style.scss":
/*!************************************!*\
  !*** ./assets/src/sass/style.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=main.js.map